package cl.duoc.login;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsuario, etPass;
    private Button btnEntrar,btnDir;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etPass = (EditText) findViewById(R.id.etPass);
        btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnDir=(Button)findViewById(R.id.btnDir);

        btnDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(LoginActivity.this,RegistroActivity.class);
                startActivity(i);
            }
        });
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });


    }

private void ingresar(){

    if(etUsuario.getText().toString().length()>0 && etPass.getText().toString().length()>0 && etUsuario.getText().toString().equals("admin") &&
            etPass.getText().toString().equals("123")){
        Toast.makeText(this, "Bienvenido: "+etUsuario.getText().toString(), Toast.LENGTH_SHORT).show();
    }else
    {
        Toast.makeText(this, "Error datos no validos", Toast.LENGTH_SHORT).show();
    }
    }

}


