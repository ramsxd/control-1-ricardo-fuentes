package cl.duoc.login;

import android.app.DatePickerDialog;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Calendar;

import cl.duoc.login.Entidades.Registro;
import cl.duoc.login.bd.FormularioRegistro;

public class RegistroActivity extends AppCompatActivity {
    private EditText etNUsuario, etNombres, etApellidos, etRut, etPassword, etRPassword;
    private Button btnRegistrar, btnFecha;
 
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        etNUsuario = (EditText) findViewById(R.id.etNUsuario);
        etNombres = (EditText) findViewById(R.id.etNombres);
        etApellidos = (EditText) findViewById(R.id.etApellidos);
        etRut = (EditText) findViewById(R.id.etRut);
        btnFecha = (Button) findViewById(R.id.btnFecha);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etRPassword = (EditText) findViewById(R.id.etRepetirP);


        btnRegistrar = (Button) findViewById(R.id.btnRegistarse);

        btnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resgistrarUsuario();
            }
        });
       
    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                btnFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }
    private void resgistrarUsuario() {
        if (etNUsuario.getText().toString().length() > 0 && etNombres.getText().toString().length()>0
              && etApellidos.getText().toString().length()>0&& etRut.getText().toString().length()>0
                && etPassword.getText().toString().length()>0 && etRPassword.getText().toString().length()>0)
        if(etPassword.getText().toString().equals(etRPassword.getText().toString()))
        {
            Registro r = new Registro();
            r.setNombres(etNombres.getText().toString());
            r.setnUsuario(etNUsuario.getText().toString());
            r.setApellidos(etApellidos.getText().toString());
            r.setRut(etRut.getText().toString());
            r.setfNacimiento(btnFecha.getText().toString());
            r.setPassword(etPassword.getText().toString());
            r.setrPassword(etRPassword.getText().toString());

            FormularioRegistro.registrarUsuario(r);
            Toast.makeText(this, "Usuario Registrado", Toast.LENGTH_SHORT).show();

        }
        else{
            Toast.makeText(this, "Contraseña deben ser iguales", Toast.LENGTH_SHORT).show();

        }

        else
        {
            Toast.makeText(this, "Todos los campos son requeridos", Toast.LENGTH_SHORT).show();
        }
    }


   
}
